import caldav
import datetime

class Caldavedy:

  def __init__(self,caldav_server):
  
    client = caldav.DAVClient(url=caldav_server['url'], username=caldav_server['username'], password=caldav_server['password'])
    #client = caldav.DAVClient(url=caldav_server[0]['url'], username=caldav_server[0]['username'], password=caldav_server[0]['password'])
    principal = client.principal()
    self.calendars = principal.calendars()

    def getKey(item):
      return item.name

    self.calendars=sorted(self.calendars, reverse=True, key=getKey)

  def print_calendars(self):
    if self.calendars:
      for calendar in self.calendars:
        #results = calendar.date_search(datetime.now(),datetime.now())
        print("%s " % (calendar.name))

  def get_calendars(self):
    ret=[]
    if self.calendars:
      for calendar in self.calendars:
        ret.append(calendar.name)
    return ret

  def get_calendar_name(self,dt):

    for calendar in self.calendars:
      if calendar.name[0].isdigit():
        results = calendar.date_search(dt,dt)
        if results:
          break

    return calendar.name

  def get_calendar_extra_name(self,dt):

    ret = [ ]
    for calendar in self.calendars:
      if not calendar.name[0].isdigit():
        results = calendar.date_search(dt,dt)
        if results:
          ret.append(calendar.name)
    
    return ret

