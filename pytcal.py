import pprint
import os

import datetime
from datetime import timedelta

import caldavedy
import utilsedy
import tempedy

import argparse

parser = argparse.ArgumentParser(description='pyCal_core : traitement')
parser.add_argument('-s', '--simul', help='mode simulation', action="store_true")
args = parser.parse_args()

repapp = os.path.dirname(os.path.realpath(__file__)) + '/'

config = utilsedy.UtilConfig(repapp +'config.json')

configPytcal = utilsedy.ConfigPytcal(repapp,config)

config_d = repapp + config.config['config_d']
config_mode = config.config['mode']

configCaldav = configPytcal.configCaldav
configIdxDomoticz = configPytcal.configIdxDomoticz
configModes = configPytcal.configModes
configPlanning = configPytcal.configPlanning

caldavedy = caldavedy.Caldavedy(configCaldav)

dtPlanning = datetime.datetime.now() + timedelta(minutes=config.config['timeMinDeltaPlanning'])
dtCalendar = datetime.datetime.now() + timedelta(minutes=config.config['timeMinDeltaCalendar'])

dt = datetime.datetime.now() + timedelta(hours=config.config['hoursdelta'])
#
calendar = caldavedy.get_calendar_name(dtCalendar) # ex : 05-semaine travail-02-25252525
calendextra = caldavedy.get_calendar_extra_name(dtCalendar)
#
dtaff = dt - timedelta(hours=config.config['hoursdelta'])
print(f'### {dtaff.strftime("%a %d/%m/%Y %H:%M")} -> {calendar}')
#
tempedy = tempedy.Tempedy(calendar,dtPlanning,configModes,configPlanning,configIdxDomoticz,calendextra)
print(f'# {tempedy.vals}')
# boucle sur zones
for zone in tempedy.vals:
  # boucle sur chauffage(s) zone
  for idx in zone[1]:
    #mqttshell="mosquitto_pub -h 127.0.0.1 -t domoticz/in -m  '{{ \"command\": \"switchlight\", \"idx\" : {idx:d}, \"switchcmd\": \"Set Level\" , \"level\" : {val:d} }}'".format(idx = idx, val = zone[2])
    mqttshell="mosquitto_pub -h 127.0.0.1 -u pytcal -P 'm7xA*wzNJM9G@Pa%FybNYJMseyEkC' -t domoticz/in -m  '{{ \"command\": \"switchlight\", \"idx\" : {idx:d}, \"switchcmd\": \"Set Level\" , \"level\" : {val:d} }}'".format(idx = idx, val = zone[2])
    # idx actif
    if idx >=0:
      # mode traitement
      if not args.simul:
        print(mqttshell)
        os.system(mqttshell)
      # mode simulation
      else:
        print(f'# simul :  {mqttshell}')
    # idx non actif
    else:
      print (f'# no send -> {mqttshell}')
