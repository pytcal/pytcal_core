class Tempedy:

  vals = []
  # TODO add param titre evenement
  def __init__(self,calendar,dt,config_mode,config_planning,idx_domoticz, calendarextra = "" ):

    self.vals = []
    calvars = calendar.split('-') # split nom calandar en cours ex: ['05', 'semaine travail', '02', '25252525']
    modes=list(calvars[3]) # conversion en structure de données ex: ['2', '5', '2', '5', '2', '5', '2', '5']
    config_semaine=config_planning[calvars[2]][dt.weekday()+1] # heures activation du jours  ex: [['dimanche'], [['06:30', '20:00']], ...
    zone=0
    # boucle sur heures activation
    for config_s in config_semaine:
      if zone != 0: # ignore [0] : ex: ['dimanche']
        idx=idx_domoticz[zone-1] # idx domotique chauffage
        tm0 = 'm' + modes[2*(zone-1)] # mode inactif
        tm1 = 'm' + modes[2*(zone-1)+1] # mode actif 
        md=config_mode[tm0]['val'] # mode par default
        for tps in config_s: # boucle sur heures activation ex : [['06:30', '20:00']]
          # si activation
          if dt.strftime('%H:%M') >= tps[0] and dt.strftime('%H:%M') < tps[1]:
            md=config_mode[tm1]['val']
        #mqttshell = ("mosquitto_pub -h 127.0.0.1 -t domoticz/in -m  '{ \"idx\" : %d , \"nvalue\" : 0, \"svalue\" : \"%d\"}'" % ( idx, md ) )
        self.vals.append(['n',idx,md, []])
      zone+=1
    # calcul extra on
    for calextra in calendarextra:
      if calextra != '':
        calextravars = calextra.split('-')
        if ( calextravars[2] == 'xx' ):
          z=0
          for val in self.vals:
            if calextravars[3][z] != 'x':
              # set type extra calendar
              val[0]='x'
              # set value zone mode 
              val[2]=config_mode[f'm{calextravars[3][z]}']['val']
              val[3]=calextra
            z=z+1
        if ( calextravars[2] == 'xy' ):
          print (config_mode[f'm4']['val'])
