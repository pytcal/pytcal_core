import json,os

class UtilConfig:

  config = {}
  config0 = {}

  def __init__(self,file,mode=None):
    rr =  file

    with open(file, 'r') as importfile:
      data=importfile.read()
      importfile.close()
      self.config0 = json.loads(data)
    if mode == None:
      self.config = self.config0
    else:
      self.config = self.config0['default']
      for idx in self.config0[mode]:
        self.config0['default'][idx] = self.config0[mode][idx] 

class ConfigPytcal:

  configPycal = {}
  configPycal = {}
  configCaldav = {}
  configIdxDomoticz = {} 
  configModes = {}
  configPlanning = {}

  def __init__(self,repconfig,config):

    config_mode = config.config['mode']
    rconf = repconfig + config.config['config_d']
    self.configPycal = UtilConfig(rconf + config.config['config_f'], config_mode).config
    self.configCaldav = UtilConfig(rconf + self.configPycal['config_f_caldav'], config_mode).config
    self.configIdxDomoticz = UtilConfig(rconf + self.configPycal['config_f_idx_domoticz']).config
    self.configModes = UtilConfig(rconf + self.configPycal['config_f_modes']).config
    self.configPlanning = UtilConfig(rconf + self.configPycal['config_f_planning']).config
    